﻿using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    public struct DirectionalLightSource
    {
        public Vector3 diffuseColor;    // the main color of the light
        public Vector3 specularColor;   // highlight color
        public Vector3 direction;       // direction the light is shining in
    }
}
